<?php

use yii\web\DbSession;
use yii\db\Migration;

/**
 * Handles the creation of table `session`.
 */
class m170906_094339_create_session_table extends Migration
{
    /**
     * @Create tb 'session'.
     */
    public function safeUp()
    {
        $this->createTable('session', [
            'id' => $this->primaryKey(),
            'id' => $this->string(40)->notNull(),
            'expire' => $this->integer(),
            'data' => $this->binary(),
        ]);
    }

    /**
     * @Drop tb 'session'.
     */
    public function safeDown()
    {
        $this->dropTable('session');
    }
}
