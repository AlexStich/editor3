$(document).ready(function ()
{
    var host = location.host; 
    var path = location.pathname;
    var statusCrop = 0;
    var pos = $('#image').offset();
    var posXY = [];
    var posMouseY1 =  posY1 = pos.top;
    var posMouseX1 =  posX1 = pos.left;
    var posMouseY2 =  posY2 = posY1 + 500;
    var posMouseX2 = posX2 = posX1 + 700;
    var press = 0; // State of push mouse button
    var X = 0;
    var Y = 0;
    var W = 0;
    var H = 0;
    
    /* Off drag mode for elements. */
    
    $('body').ondragstart = function ()
    {
        return false;
    };

    /* Handler selection area for crop. */

    $('#image').on('mousedown', function (e_down)
    {
        if ($('#crop').prop('checked')) {
            posMouseY1 = e_down.pageY;
            posMouseX1 = e_down.pageX;
            press = 1;
        }
    })

        .on('mousemove', function (e_move)
        {
            if ($('#crop').prop('checked') && (press === 1)) {
                posMouseY2 = e_move.pageY;
                posMouseX2 = e_move.pageX;
                $('#box1,#box2,#box3,#box4').detach();    
                posXY = PositionDisplay(posMouseY1,posMouseX1,posMouseY2,posMouseX2);
                htmlTagImg = HtmlImgDisplay (posXY);      
                $('.display-mask').html(htmlTagImg);
            }
        })

        .on('mouseup', function (e_up)
        {
            press = 0;
        });
            
    /* Calculation area for shadow. */
            
    function  PositionDisplay (posMouseY1,posMouseX1,posMouseY2,posMouseX2)
    {
        var pos = $('#image').offset();
        var H0 = $('#image').height();
        var W0 = $('#image').width();
        var posY0 =  pos.top;
        var posX0 =  pos.left;
        var posMaxY0 =  posY0 + H0;
        var posMaxX0 =  posX0 + W0;
        
            if (posMouseY1 <= posY0){posMouseY1 = posY0;}
            if (posMouseY1 >= posMaxY0){posMouseY1 = posMaxY0;}
            if (posMouseX1 <= posX0){posMouseX1 = posY0;}
            if (posMouseX1 >= posMaxX0){posMouseX1 = posMaxY0;}
            if (posMouseY2 <= posY0){posMouseY2  = posY0;}
            if (posMouseY2 >= posMaxY0){posMouseY2  = posMaxY0;}
            if (posMouseX2 <= posX0){posMouseX2  = posY0;}
            if (posMouseX2 >= posMaxX0){posMouseX2  = posMaxY0;}

            if(posMouseX1 >= posMouseX2){
                posX1 = posMouseX2; posX2 = posMouseX1; 
            }else {posX2 = posMouseX2; posX1 = posMouseX1;}

            if(posMouseY1 >= posMouseY2){
                posY1 = posMouseY2; posY2 = posMouseY1; 
            }else {
                posY2 = posMouseY2; posY1 = posMouseY1;
            }
            
            posBox1 = [0,0,(posX1-posX0),(posMaxY0-posY0)]; 
            posBox2 = [posX1-posX0,posY0-posY0,(posX2-posX1),(posY1-posY0)]; 
            posBox3 = [posX1-posX0,posY2-posY0,(posX2-posX1),(posMaxY0-posY2)]; 
            posBox4 = [posX2-posX0,posY0-posY0,(posMaxX0-posX2),(posMaxY0-posY0)]; 

            posXY = [posBox1,posBox2,posBox3,posBox4];

            return (posXY);
    }
    
    /* Perpare tags of shadow for insert to HTML. */
    
    function HtmlImgDisplay (posXY)
    {
        var style = 'style = "opacity: 0.5; z-index = 2; position: absolute; ';
        var style1 = style + 'left: ' + posXY [0][0] + 'px; top: ' + posXY [0][1] + 'px; width: ' + posXY [0][2] + 'px; height: ' + + posXY [0][3] + 'px;"'; 
        var style2 = style + 'left: ' + posXY [1][0] + 'px; top: ' + posXY [1][1] + 'px; width: ' + posXY [1][2] + 'px; height: ' + + posXY [1][3] + 'px;"';
        var style3 = style + 'left: ' + posXY [2][0] + 'px; top: ' + posXY [2][1] + 'px; width: ' + posXY [2][2] + 'px; height: ' + + posXY [2][3] + 'px;"';
        var style4 = style + 'left: ' + posXY [3][0] + 'px; top: ' + posXY [3][1] + 'px; width: ' + posXY [3][2] + 'px; height: ' + + posXY [3][3] + 'px;"';
        
        var htmlTagImg = '<img id="box1" class="visible-mask" src="/basic/web/uploads/mask.jpg" \n\
                    alt=" " ondrag="return false" ondragdrop="return false" \n\
                    ondragstart="return false" '+ style1 + ' >' + 
                    '<img id="box2" class="visible-mask" src="/basic/web/uploads/mask.jpg" \n\
                    alt=" " ondrag="return false" ondragdrop="return false" \n\
                    ondragstart="return false" '+ style2 + ' >' +
                    '<img id="box3" class="visible-mask" src="/basic/web/uploads/mask.jpg" \n\
                    alt=" " ondrag="return false" ondragdrop="return false" \n\
                    ondragstart="return false" '+ style3 + ' >' +
                    '<img id="box4" class="visible-mask" src="/basic/web/uploads/mask.jpg" \n\
                    alt=" " ondrag="return false" ondragdrop="return false" \n\
                    ondragstart="return false" '+ style4 + ' >';
        
        return htmlTagImg;
    };
    
    /* Off crop mode. */
   
    $('#crop').on( 'change', function()
    {
        if ($(this).prop('checked')) {
            statusCrop = 1;
            }else{
               statusCrop = 0; 
               $('#box1, #box2, #box3, #box4').detach();  
            }
    });
    
    /* Button save crop. Send coordinate of crop area to action Edit. */
    
    $('#save').on('click', function ()
    {
        var X = posXY[1][0];
        var Y = posXY[1][3];
        var W = posXY[1][2];
        var H = (posXY[2][1] - posXY[1][3]);
        if (statusCrop === 1) {
            document.location.href =(path+'?crop=1&X=' + X + '&Y=' + Y + '&W=' + W + '&H=' + H);
        }
    });
    
    /* Button inverse. Send subbmit inverse to action Edit. */
        
    $('#inverse').on('click', function ()
    {
        var inverse = 1;
        var host = location.host;
        var path = location.pathname;
        document.location.href =(path+'?inverse=1');
    });
});
    
    

