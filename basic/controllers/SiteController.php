<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\LoadForm;
use yii\web\UploadedFile;
use app\models\UrlForm;

    /**
     * Main controller
     */
class SiteController extends Controller
{
    /**
     * Action of Home page. Prepare load forms file and URL.
     * If there is subbmit of load file than redirect to Edit action.
     * Render Home.
     */
    public function actionIndex()
    {
        Yii::$app->session->open(); 
        $model = new LoadForm();
        $modelUrl = new UrlForm();

        $conditionPost = Yii::$app->request->isPost;
        $modelUrl->load(Yii::$app->request->post());
        $source_url = $modelUrl->sourceUrl;
        
        if ($conditionPost && !$source_url) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->upload()) {
                return $this->redirect(['edit']);
            }
        } elseif ($conditionPost && $source_url) { 
            if ($modelUrl->upload()){
                return $this->redirect(['edit']);
            }
        }
        return $this->render('index', ['model' => $model, 'modelUrl' => $modelUrl]);
    }
    
    /**
     * Action of Edit window. For operate location file use session data.
     * Handles the request. 
     * Interacts with the component PictureImage for edit image.
     * Render Edit Window.
     */
    public function actionEdit()
    {
       
        Yii::$app->session->open(); 
        
        /* Check sesion data actual. */
        
        $path_edit = Yii::$app->session->get('path_edit');
        if(!$path_edit){return $this->render('nodate');}
        
        /* Crop image. */
        
        if (Yii::$app->request->get('crop')) {
            $request = Yii::$app->request; 
            $coordinateX = $request->get('X');
            $coordinateY = $request->get('Y');
            $width = $request->get('W');
            $height = $request->get('H');
            Yii::$app->pictureManager->crop($path_edit,$width,$height,$coordinateX,$coordinateY);
        }
        
        /* Inverse image.*/
        
        if (Yii::$app->request->get('inverse')) {
            Yii::$app->pictureManager->inverse($path_edit);
        }
        
        /* Resize image before render. */
        
        $sizeImage = Yii::$app->pictureManager->resize($path_edit);
        
        return $this->render('edit', ['path_edit' => $path_edit, 'sizeImage' => $sizeImage]);
    }
}
