<?php

namespace app\components;

use yii\base\Component;
use yii\imagine\Image;
use Imagine\Image\Box;

/**
 * Component edit image. Crop, resize, inverse.
 * 
 * @var int $widht_resize  * 
 */
class PictureManager extends Component 
{ 
        public $width_resize = 700;
        
    /**
     * Crop image and save.
     *  
     * @param string $path_edit Image path directory 
     * @param float $width Needed width
     * @param float $height Needed heidht
     * @param float $coordinateX Zero point
     * @param float $coordinatey Zero point
     */    
    public function crop($path_edit, $width, $height, $coordinateX, $coordinateY)
    {
        Image::crop($path_edit, $width, $height, [$coordinateX, $coordinateY])
            ->save($path_edit);
    }

   /**
     * Resize image to widht = $widht_resize and save to server.
     * Default widht = 700. Resize with ratio.
     * 
     * @param string $path_edit Image path directory 
     * @param $widht_resize Needed widht
    * 
     * @return Array of new size image - widht and height
     */    
    public function resize($path_edit, $width_resize = 700)
    {
        $image = Image::getImagine()->open($path_edit);
        $size = $image->getSize();
        $ratio = $size->getWidth() / $size->getHeight();
        $height = round($width_resize / $ratio);
        $box = new Box($width_resize, $height);
        $image->resize($box)->save($path_edit);
        return array($width_resize, $height);
    }

    /**
     * Inverse image and save.
     *  
     * @param string $path_edit Image path directory 
     */  
    public function inverse($path_edit)
    {
        $image = Image::getImagine();
        $newImage = $image->open($path_edit);
        $newImage->effects()->negative()->gamma(1.3);
        $newImage->save($path_edit);
    }

}

