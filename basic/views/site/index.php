<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Редактор изображений';
?>

<div class="container">
    <div class="body-content">

        <!-- Load file form -->

        <div class="d-inline-flex">

            <div class="border border-secondary p-3 m-1" style = "border-radius: 10px">

                <?php   $form = ActiveForm::begin([
                            'layout' => 'default',
                            'options' => [
                                'enctype' => 'multipart/form-data'
                            ],
                        ]); 
                echo $form->field($model, 'file')
                         ->label(false)
                         ->fileInput();
                echo Html::submitButton('Загрузить', ['class' => 'btn btn-primary']);
                ActiveForm::end(); ?>
                
            </div>

            <!-- Load URL form -->

            <div class="border border-secondary p-3 m-1" style = "border-radius: 10px">
                <?php $formUrl = ActiveForm::begin([
                          'layout' => 'default',
                          'options' => ['enctype' => 'multipart/form-data']
                      ]);
                echo $formUrl->field($modelUrl, 'sourceUrl')
                        ->label(false)
                        ->textInput();
                echo Html::submitButton('Загрузить URL', [
                         'class' => 'btn btn-primary',
                         'placeholder' => 'Введите URL'
                     ]);
                ActiveForm::end();
                ?>
            </div>
        </div>

        <!-- Edit window -->

        <div class="display-area m-1">
            <?php echo Html::img('@web/uploads/fon.jpg', [
                           'class' => 'border border-primary',
                           'width' => 700,
                           'height' => 500,
                           'id' => 'image'
                       ]); ?>
        </div>
        
        <!-- Redirect Last edit -->
        
        <div class="border border-secondary p-3 col-md-6 m-1" style = "border-radius: 10px">   
            <?php echo Html::a('Последняя редакция', ['site/edit'], ['class' => 'btn btn-primary']); ?>
        </div>
    </div>
</div>

