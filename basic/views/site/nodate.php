<?php

use yii\helpers\Html;

$this->title = 'EDIT';
?>

<div class="container">
    <div class="body-content">
        <div class=" d-flex flex-column">
            <div class="p-1 m-1">
                <h2> Нет сохранненых данных последней редакции.</h2>
            </div>
            <br>
            <div class="p-1 m-1">
                <?= Html::a('Загрузить в редактор', ['index'], ['class' => 'btn btn-secondary']) ?>
            </div>
        </div>
    </div>
</div>