<?php

use yii\bootstrap\Button;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Редактор изображений';
?>

<div class="body-content">
    <div class="container">

        <!-- Button upload, download files -->

        <div class="d-inline-flex">
            <div class="p-1 m-1">
                <?= Html::a('Загрузить в редактор', ['index'], ['class' => 'btn btn-secondary']); ?>
            </div>
            <div class="p-1 m-1">
                <?= Html::a('Выгрузить из редактора', Url::to('@web/' . $path_edit), ['class' => 'btn btn-secondary', 'download' => '']) ?>
            </div>
        </div>

        <!-- Edit windows -->

        <div  class="display-area m-1" id="image" style = "padding-left: 0px; opacity: 1; position: relative; z-index: 3; width: <?= $sizeImage[0] ?>px;">
            <?php echo Html::img(Url::to('@web/' . $path_edit), [
                           'class' => 'border border-secondary',
                           'width' => $sizeImage[0],
                           'id' => 'image-image',
                           'ondrag' => 'return false',
                           'ondragdrop' => 'return false',
                           'ondragstart' => 'return false',
                           'z-index' => '1',
                           'position' => 'relative']); ?>

            <!-- Mask area -->

            <div class="display-mask m-1">
            </div> 
        </div>

        <!-- Edit buttons  -->

        <div class="form-group d-inline-flex">

            <!-- Crop buttons -->

            <div class="border border-secondary p-2 m-1 d-flex flex-column" style = "padding-left: 0px">
                <div class="btn-group m-1" data-toggle="buttons">
                    <?php echo Html::checkbox('Включить режим обрезки', false, [
                                   'label' => 'Включить/Отключить режим обрезки',
                                   'labelOptions' => [
                                       'class' => 'btn btn-success',
                                       'id' => 'stateCrop',
                                   ],
                                   'class' => 'btn btn-success',
                                   'id' => 'crop',
                                   'autocomplete' => 'off',
                               ]); ?>
                </div>
                <div class="btn-group m-1">
                    <?php echo Button::widget(['label' => 'Обрезать', 'options' => ['class' => 'btn btn-success', 'id' => 'save']]) ?>
                </div>
            </div>
            
            <!-- Inverse button -->

            <div class="border border-secondary p-3 m-1" style = "padding-left: 0px">
                <?php echo Button::widget(['label' => 'Инвертировать', 'options' => ['class' => 'btn btn-success', 'id' => 'inverse']]); ?>
            </div>   
        </div>  







