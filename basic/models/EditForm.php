<?php

namespace app\models;

use yii\base\Model;

/* Модель для кнопок обрезки. */

Class EditForm extends Model
{
    public $crop = false;
    public $inverse = false;
  
}