<?php

namespace app\models;

use yii\base\Model;
use yii\imagine\Image;
use app\models\SessionData;
use Yii;

/**
 * Model for URL Load form. 
 * Takes URL input file. Validate URL, upload file and save to session data.
 * Use SesionData component and Image class.
 */
class UrlForm extends Model
{
    public $sourceUrl;

    public function rules()
    {
        return [
           [['sourceUrl'], 'url', 'skipOnEmpty' => true, 'defaultScheme' => 'http'],
        ];
    }
    
    public function upload()
    {
        if ($this->validate()) {
            $session_id = Yii::$app->session->id;    
            $path = 'uploads/'.$session_id.'_url.jpg';
            $path_edit = 'uploads/'.$session_id.'_url_edit.jpg';
            $image = Image::getImagine()->open($this->sourceUrl);
            $image->save($path);
            copy($path, $path_edit);
            
            SessionData::setParam($path, $path_edit);
            
            return true;
        } else { 
            return false;
        }
    }

}
