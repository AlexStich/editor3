<?php

namespace app\models;

use yii\web\dbSession;
use Yii;

/**
 * Set data to session.
 * @var $path Path to location initial file
 * @var $path_edit Path to edit file 
 */
class SessionData extends dbSession
{
    public static function setParam($path='uploads/fon.jpg',$path_edit='uploads/fon.jpg')
    {
        $session = Yii::$app->session;
        $session->set('path', $path);
        $session->set('path_edit', $path_edit);
    }
}