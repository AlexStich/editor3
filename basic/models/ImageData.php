<?php

namespace app\models;

class ImageData extends \yii\db\ActiveRecord
{
    
    /* Проверка на наличие строки таблицы*/
    public function check()
    {
        if(empty($this -> find()->limit(1)->one()))
        {
            $this -> id = '1';
            $this -> i_path = 'uploads/fon.jpg';
            $this -> i_url = '';
            $this -> c_path = 'uploads/fon.jpg';
            $this -> date = null;
            $this-> save();
            
        }   
       return true;  
    }

     public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'i_path' => 'PATH',
            'i_url' => 'URL',            
            'c_path' => 'CURRENT PATH',
            'date' => 'DATA',
        ];
    }
    
}