<?php

namespace app\models;

use yii\base\Model;
use app\models\SessionData;
use Yii;

/**
 * Model for Load form. 
 * Takes input file. Validate, upload and save to session data.
 * Use SesionData component.
 */
class LoadForm extends Model
{
    public $file;

    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => false, 'mimeTypes' => 'image/jpeg, image/png', 'maxSize' => 26214400],
        ];
    }


    public function upload()
    {
        if ($this->validate()) {
            $session_id = Yii::$app->session->id;
            $path = ('uploads/' .$session_id.'_'.$this->file->baseName.'.'.$this->file->extension);
            $path_edit = ('uploads/'.$session_id.'_'.$this->file->baseName.'_edit.'.$this->file->extension);
            $this->file->saveAs($path);
            copy($path, $path_edit);
            
            SessionData::setParam($path, $path_edit);
            
            return true;
        } else {
            return false;
        }
    }
}